<?php

namespace Drupal\simple_entity_selection\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'simple entity selection' entity_reference.
 *
 * @EntityReferenceSelection(
 *   id = "simple_entity_selection",
 *   label = @Translation("Simple entity selection: Filter by all available entities"),
 *   group = "simple_entity_selection"
 * )
 */
class SimpleEntitySelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'ids' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $all_entities = \Drupal::entityTypeManager()->getStorage($configuration['target_type'])->loadMultiple();
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['ids'] = [
      '#type' => 'select',
      '#options' => array_map(fn (EntityInterface $e) => $e->label(), $all_entities),
      '#default_value' => $configuration['ids'],
      '#multiple' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);
    $configuration = $this->getConfiguration();
    if ($ids = $configuration['ids']) {
      $entity_type = $this->entityTypeManager->getDefinition($configuration['target_type']);
      $query->condition($entity_type->getKey('id'), $ids, 'IN');
    }
    return $query;
  }

}
